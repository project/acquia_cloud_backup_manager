<?php

namespace Drupal\acquia_cloud_backup_manager\Form;

use Drupal\acquia_cloud_backup_manager\AcquiaCloudClient;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure acquia_cloud_backup_manager settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * Acquia cloud client.
   *
   * @var \Drupal\acquia_cloud_backup_manager\AcquiaCloudClient
   */
  protected AcquiaCloudClient $acquiaCloudClient;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'acquia_cloud_backup_manager_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['acquia_cloud_backup_manager.settings'];
  }

  /**
   * Inject dependencies and call parent construct.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\acquia_cloud_backup_manager\AcquiaCloudClient $acquia_cloud_client
   *   Acquia cloud client.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AcquiaCloudClient $acquia_cloud_client) {
    parent::__construct($config_factory);
    $this->acquiaCloudClient = $acquia_cloud_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('acquia_cloud.client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('acquia_cloud_backup_manager.settings');
    $form['credentials'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Acquia Cloud API Credentials'),
    ];
    if (!$this->acquiaCloudClient->environmentVariablesAvailables()) {
      // Get the form values and raw input (unvalidated values).
      $key = $form_state->getValue('key') ?? $config->get('key');
      $secret = $form_state->getValue('secret') ?? $config->get('secret');

      $form['credentials']['description'] = [
        '#markup' => '<p>' . $this->t('The API credentials can be generated following <a href="https://docs.acquia.com/cloud-platform/develop/api/auth/">the Acquia Cloud Documentation</a>') . '</p>' .
        '<p>Also the API credentials can be defined as environment variables as "CLOUD_PLATFORM_API_TOKEN" and "CLOUD_PLATFORM_API_SECRET" see the <a href="https://docs.acquia.com/cloud-platform/manage/variables/">the Acquia Cloud Documentation</a></p>',
      ];

      $form['credentials']['key'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Key'),
        '#default_value' => $key,
      ];
      $form['credentials']['secret'] = [
        '#type' => 'textfield',
        '#required' => TRUE,
        '#title' => $this->t('Secret'),
        '#default_value' => $secret,
      ];
    }
    else {
      $form['credentials']['message'] = [
        '#markup' => '<p>' . $this->t('The fields for API credentials are hidden because are set globally') . '</p>',
      ];
    }

    $application = $config->get('application_uuid');
    $environment = $config->get('environment_uuid');
    $database_name = $config->get('database_name');
    $keep_options = $config->get('keep_limit_type');

    // Define a wrapper id to populate new content into.
    $ajax_wrapper = 'ajax-wrapper-backup-manager-applications';

    $form['show'] = [
      '#type' => 'submit',
      '#submit' => [[$this, 'showSubmitHandler']],
      '#executes_submit_callback' => FALSE,
      '#description' => $this->t('The applications only will be shown if the Api credentials are valid.'),
      '#value' => $this->t('Load Applications and Environments'),
      '#url' => Url::fromRoute('<current>'),
      '#ajax' => [
        'callback' => [$this, 'showApplicationsAjax'],
        'method' => 'replace',
        'wrapper' => $ajax_wrapper,
      ],
    ];

    $form['applications'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $ajax_wrapper,
      ],
    ];

    $form['applications']['application_uuid'] = [
      '#type' => 'select',
      '#title' => $this->t('Application'),
      '#empty_option' => $this->t('- Select a value -'),
      '#default_value' => $application,
      '#validated' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'showApplicationsAjax'],
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => 'ajax-wrapper-backup-manager-applications',
      ],
    ];

    $application_uuid_options = [];

    if ((!empty($key) && !empty($secret)) || $this->acquiaCloudClient->environmentVariablesAvailables()) {
      $application_uuid_options = $this->getApplicationOptions();
      $form['applications']['application_uuid']['#options'] = $application_uuid_options;
    }

    $form['applications']['environment_uuid'] = [
      '#type' => 'select',
      '#title' => $this->t('Environment'),
      '#validated' => TRUE,
      '#default_value' => $environment,
      '#empty_option' => $this->t('- Select a value -'),
    ];

    if (!empty($application) || !empty($application_uuid_options)) {
      if (empty($application)) {
        $application = current(array_keys($application_uuid_options));
      }
      $environments_uuid_options = $this->getEnvironmentOptions($application);
      $form['applications']['environment_uuid']['#options'] = $environments_uuid_options;
    }

    $form['applications']['database_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Database'),
      '#validated' => TRUE,
      '#default_value' => $database_name,
      '#empty_option' => $this->t('- Select a value -'),
    ];

    if (!empty($application)) {
      $databases_options = $this->getDatabaseOptions($application);
      $form['applications']['database_name']['#options'] = $databases_options;
    }

    $form['cron'] = [
      '#type' => 'fieldset',
    ];

    $form['cron']['cron_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable cron'),
      '#default_value' => $config->get('cron_enabled'),
    ];

    $form['cron']['keep_limit_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Keep backup options'),
      '#default_value' => $keep_options,
      '#required' => TRUE,
      '#options' => [
        'number_to_keep' => $this->t('Number of backups to keep'),
        'time_to_keep' => $this->t('Time limit to keep backups'),
      ],
    ];

    $form['cron']['number_to_keep'] = [
      '#type' => 'number',
      '#title' => $this->t('Number of backups to keep.'),
      '#default_value' => $config->get('number_to_keep'),
      '#states' => [
        'visible' => [
          'select[name="keep_limit_type"]' => ['value' => 'number_to_keep'],
        ],
        'required' => [
          'select[name="keep_limit_type"]' => ['value' => 'number_to_keep'],
        ],
      ],
    ];

    $form['cron']['time_to_keep'] = [
      '#type' => 'number',
      '#title' => $this->t('Time to keep the backups in days.'),
      '#default_value' => $config->get('time_to_keep'),
      '#states' => [
        'visible' => [
          'select[name="keep_limit_type"]' => ['value' => 'time_to_keep'],
        ],
        'required' => [
          'select[name="keep_limit_type"]' => ['value' => 'time_to_keep'],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->config('acquia_cloud_backup_manager.settings')
      ->set('key', $form_state->getValue('key'))
      ->set('secret', $form_state->getValue('secret'))
      ->set('application_uuid', $form_state->getValue('application_uuid'))
      ->set('environment_uuid', $form_state->getValue('environment_uuid'))
      ->set('cron_enabled', $form_state->getValue('cron_enabled'))
      ->set('keep_limit_type', $form_state->getValue('keep_limit_type'))
      ->set('number_to_keep', $form_state->getValue('number_to_keep'))
      ->set('time_to_keep', $form_state->getValue('time_to_keep'))
      ->set('database_name', $form_state->getValue('database_name'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!$this->acquiaCloudClient->environmentVariablesAvailables()) {
      $key = $form_state->getValue('key');
      if (empty($key)) {
        $form_state->setError($form, $this->t('Key is required.'));
      }
      $secret = $form_state->getValue('secret');
      if (empty($secret)) {
        $form_state->setError($form, $this->t('Secret is required.'));
      }
      $this->acquiaCloudClient->setClient($key, $secret);
    }

    if (empty($form_state->getErrors())) {

      // Show message error if credentials are not valid.
      try {
        $applications = $this->getApplicationOptions();
        $form_state->set('applications', $applications);
      }
      catch (IdentityProviderException $e) {
        $form_state->setError($form, $this->t('Invalid credentials.'));
      }

      if (empty($applications)) {
        $form_state->setError($form, $this->t('Could not retrieve any application, try again or review you credentials.'));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * Load Acquia cloud select options.
   */
  public function showApplicationsAjax(array $form, FormStateInterface $form_state) {
    $applications = $form_state->get('applications');
    if (!empty($applications)) {
      $form['applications']['application_uuid']['#options'] = $applications;

      $application = $form_state->get('application');
      if (empty($application)) {
        $applications_ids = array_keys($applications);
        $application = current($applications_ids);
      }

      $environments = $this->getEnvironmentOptions($application);
      $form['applications']['environment_uuid']['#options'] = $environments;

      $databases = $this->getDatabaseOptions($application);
      $form['applications']['database_name']['#options'] = $databases;

    }

    // Return the element that will replace the wrapper (we return itself).
    return $form['applications'];
  }

  /**
   * Laod application options.
   */
  private function getApplicationOptions() {
    $applications = [];
    try {
      $applications = $this->acquiaCloudClient->getApplicationList();
    }
    catch (\Exception $e) {
      // Do nothing if the client failed.
    }
    return $applications;
  }

  /**
   * Load environment options.
   */
  private function getEnvironmentOptions(string $application) {
    $environments = [];
    try {
      $environments = $this->acquiaCloudClient->getEnvironmentList($application);
    }
    catch (\Exception $e) {
      // Do nothing if the client failed.
    }

    return $environments;
  }

  /**
   * Get the list option from application id.
   */
  private function getDatabaseOptions(string $application) {
    $databases = [];
    try {
      $databases = $this->acquiaCloudClient->getDatabasesList($application);
    }
    catch (\Exception $e) {
      // Do nothing if the client failed.
    }

    return $databases;
  }

}
