<?php

namespace Drupal\acquia_cloud_backup_manager;

use AcquiaCloudApi\Connector\Client;
use AcquiaCloudApi\Connector\Connector;
use AcquiaCloudApi\Endpoints\Applications;
use AcquiaCloudApi\Endpoints\DatabaseBackups;
use AcquiaCloudApi\Endpoints\Databases;
use AcquiaCloudApi\Endpoints\Environments;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Acquia Cloud Client.
 */
class AcquiaCloudClient {

  /**
   * API Secret environment var.
   */
  const API_SECRET_VAR_NAME = 'CLOUD_PLATFORM_API_SECRET';

  /**
   * API Token environment var.
   */
  const API_TOKEN_VAR_NAME = 'CLOUD_PLATFORM_API_TOKEN';

  /**
   * Acquia Cloud client.
   *
   * @var \AcquiaCloudApi\Connector\Client|null
   */
  private ?Client $client = NULL;

  /**
   * Acquia cloud settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private ImmutableConfig $config;

  /**
   * Acquia Database helper.
   *
   * @var \AcquiaCloudApi\Endpoints\DatabaseBackups|null
   */
  private ?DatabaseBackups $databaseBackup = NULL;

  /**
   * Load dependencies.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get('acquia_cloud_backup_manager.settings');
  }

  /**
   * Get Acquia Cloud client.
   */
  public function getClient() {
    if (!($this->client instanceof Client)) {
      if (!(getenv(self::API_TOKEN_VAR_NAME) && getenv(self::API_SECRET_VAR_NAME))) {
        $key = $this->config->get('key');
        $secret = $this->config->get('secret');
      }
      else {
        $key = getenv(self::API_TOKEN_VAR_NAME);
        $secret = getenv(self::API_SECRET_VAR_NAME);
      }

      $config = [
        'key' => $key,
        'secret' => $secret,
      ];

      $connector = new Connector($config);
      $client = Client::factory($connector);
      $this->client = $client;
    }

    return $this->client;
  }

  /**
   * Set Acquia Database service.
   */
  public function setDatabaseService(DatabaseBackups $database_backups) {
    $this->databaseBackup = $database_backups;
  }

  /**
   * Set Acquia Cloud client.
   */
  public function setClient(Client $client) {
    $this->client = $client;
  }

  /**
   * Set Acquia Cloud Client.
   */
  public function setClientWithCredentials(string $key = '', string $secret = '') {
    $config = [
      'key' => $key,
      'secret' => $secret,
    ];

    $connector = new Connector($config);
    $client = Client::factory($connector);
    $this->client = $client;
  }

  /**
   * Get backup list.
   */
  public function getBackupList() {
    $backup = $this->getDabaseService();
    $environment_uuid = $this->config->get('environment_uuid');
    $database_name = $this->config->get('database_name');
    $backups = $backup->getAll($environment_uuid, $database_name);
    $backup_list = [];

    /** @var \AcquiaCloudApi\Response\BackupResponse $backup_item */
    foreach ($backups as $backup_item) {
      // Only treat the ondemand backups.
      if ($backup_item->type == 'ondemand') {
        $backup_list[$backup_item->id] = strtotime($backup_item->completedAt);
      }
    }
    return $backup_list;
  }

  /**
   * Get Acquia Cloud application list.
   */
  public function getApplicationList() {
    $client = $this->getClient();
    $application = new Applications($client);
    $applications_list = $application->getAll();
    $applications = [];

    /** @var \AcquiaCloudApi\Response\ApplicationResponse $application */
    foreach ($applications_list as $application) {
      $applications[$application->uuid] = $application->name;
    }
    return $applications;
  }

  /**
   * Get Acquia Cloud environment list.
   */
  public function getEnvironmentList(string $application_id) {
    $client = $this->getClient();
    $environments = new Environments($client);
    $environments_list = $environments->getAll($application_id);
    $environments = [];
    /** @var \AcquiaCloudApi\Response\EnvironmentResponse $environment */
    foreach ($environments_list as $environment) {
      $environments[$environment->uuid] = $environment->name;
    }
    return $environments;
  }

  /**
   * Ge the database list by the application id.
   */
  public function getDatabasesList(string $application_id) {
    $client = $this->getClient();
    $databases = new Databases($client);
    $databases_list = $databases->getAll($application_id);
    $databases = [];
    /** @var \AcquiaCloudApi\Response\DatabaseResponse $database */
    foreach ($databases_list as $database) {
      $databases[$database->name] = $database->name;
    }
    return $databases;
  }

  /**
   * Delete older backups.
   *
   * @param int $limit
   *   The number of days or number limit of the backups to keep.
   * @param string $type
   *   Valid types: 'number_to_keep' and 'time_to_keep'.
   */
  public function deleteOlderBackups(int $limit, string $type = 'time_to_keep') {
    $backup_list = $this->getBackupList();
    // Do not delete all backups.
    if (empty($backup_list) || count($backup_list) == 1 || $limit == 0) {
      return [];
    }

    // Sort backups by key in ascending order (older first).
    ksort($backup_list, SORT_NUMERIC);

    $backups_to_delete = [];
    $backup_count = count($backup_list);
    switch ($type) {
      case 'time_to_keep':
        $time_limit = strtotime('-' . $limit . 'days');

        foreach ($backup_list as $uuid => $backup_time) {
          if ($backup_time < $time_limit) {
            $backups_to_delete[$uuid]['date'] = date(\DateTimeInterface::ATOM, $backup_time);
          }
        }

        $backups_delete_count = count($backups_to_delete);
        // Do not delete all backups.
        if ($backups_delete_count == $backup_count) {
          // Keep the newer backup.
          return array_slice($backups_to_delete, 0, -1, TRUE);
        }

        break;

      case 'number_to_keep':
        $elements_to_delete = [];

        if ($backup_count > $limit) {
          $elements_to_delete = array_slice($backup_list, 0, -$limit, TRUE);
        }

        foreach ($elements_to_delete as $uuid => $backup_time) {
          $backups_to_delete[$uuid]['date'] = date(\DateTimeInterface::ATOM, $backup_time);
        }
        break;
    }

    $deleted_backups = [];
    if (!empty($backups_to_delete)) {
      $deleted_backups = $this->deleteBackups($backups_to_delete);
    }
    return $deleted_backups;
  }

  /**
   * Delete a list of backups by its uuid.
   */
  public function deleteBackups(array $backups_to_delete) {
    if (empty($backups_to_delete)) {
      return [];
    }

    $backups_client = $this->getDabaseService();
    $environment_uuid = $this->config->get('environment_uuid');
    $database_name = $this->config->get('database_name');

    foreach (array_keys($backups_to_delete) as $uuid) {
      $backups_client->delete($environment_uuid, $database_name, $uuid);
    }
    return $backups_to_delete;
  }

  /**
   * Check if API Credentials environment variables are available.
   */
  public function environmentVariablesAvailables() {
    return (getenv(self::API_TOKEN_VAR_NAME) && getenv(self::API_SECRET_VAR_NAME));
  }

  /**
   * Returns Acquia Cloud Database helper.
   */
  private function getDabaseService() {
    if (!$this->databaseBackup instanceof DatabaseBackups && $this->getClient()) {
      $this->databaseBackup = new DatabaseBackups($this->client);
    }
    return $this->databaseBackup;
  }

}
