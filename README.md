# Acquia Cloud - Backup Manager

Acquia Cloud helper for the platform backup management.
If you are using Acquia ACSF watch the module
[acsf_backup_manager](https://www.drupal.org/project/acsf_backup_manager)

This module allows to set a time limit to keep your manual (on-demand) backups
on Acquia Cloud, deleting by cron the older ones.
Based on the API documentation on
[Acquia Cloud API Documentation](https://cloudapi-docs.acquia.com/#/Environments/getEnvironmentsDatabaseBackups)

* [Module homepage](https://www.drupal.org/project/acquia_cloud_backup_manager)
* [Issues](https://www.drupal.org/project/issues/acquia_cloud_backup_manager)

## REQUIREMENTS

This module requires the Acquia Cloud API access, you should generate an
API key.  
[Read the Acquia documentation to generate it](https://docs.acquia.com/cloud-platform/develop/api/auth/)

## INSTALLATION
Install as you would normally install a contributed Drupal module.

## CONFIGURATION

It has a configuration page in "Configuration / Web Services /
Acquia Cloud - Backup Manager Settings"
