<?php

namespace Drupal\Tests\acquia_cloud_backup_manager\Unit;

use AcquiaCloudApi\Connector\Client;
use AcquiaCloudApi\Endpoints\DatabaseBackups;
use AcquiaCloudApi\Response\BackupsResponse;
use Drupal\acquia_cloud_backup_manager\AcquiaCloudClient;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Tests\UnitTestCase;

/**
 * Acquia Cloud Delete Service coverage.
 */
class AcquiaCloudDeleteBackupTest extends UnitTestCase {

  /**
   * Delete service coverage.
   *
   * @covers \Drupal\acquia_cloud_backup_manager\AcquiaCloudClient
   * @dataProvider getBackupsScenarios
   */
  public function testDeleteBackups(
    int $limit,
    string $type,
    string $assert_message,
    array $expected_backups_deleted) {
    $config_mock = $this->createMock(ConfigFactory::class);
    $acquia_settings_mock = $this->createMock(ImmutableConfig::class);
    $acquia_settings_mock->method('get')->willReturn('1234');
    $config_mock->method('get')->willReturn($acquia_settings_mock);
    $acquia_client = new AcquiaCloudClient($config_mock);
    $client_mock = $this->createMock(Client::class);
    $acquia_client->setClient($client_mock);
    $database_mock = $this->createMock(DatabaseBackups::class);
    $date_format = 'Y-m-d\T00:00:00P';
    $backup = [
      'id' => 42345,
      'database' => new \stdClass(),
      'type' => 'ondemand',
      'started_at' => date($date_format, strtotime('-1 day')),
      'completed_at' => date($date_format, strtotime('-1 day')),
      'flags' => new \stdClass(),
      'environment' => new \stdClass(),
      '_links' => new \stdClass(),
    ];
    $backup_responses = [];
    $backup_responses[] = (object) $backup;
    $backup['id'] = 32345;
    $backup['started_at'] = date($date_format, strtotime('-10 days'));
    $backup['completed_at'] = date($date_format, strtotime('-10 days'));
    $backup_responses[] = (object) $backup;
    $backup['id'] = 22345;
    $backup['started_at'] = date($date_format, strtotime('-15 days'));
    $backup['completed_at'] = date($date_format, strtotime('-15 days'));
    $backup_responses[] = (object) $backup;
    $backup['id'] = 12345;
    $backup['started_at'] = date($date_format, strtotime('-20 days'));
    $backup['completed_at'] = date($date_format, strtotime('-20 days'));
    $backup_responses[] = (object) $backup;
    $backup_responses_object = new BackupsResponse($backup_responses);
    $database_mock->method('getAll')->willReturn($backup_responses_object);
    $acquia_client->setDatabaseService($database_mock);

    $deleted_backups = $acquia_client->deleteOlderBackups($limit, $type);
    $this->assertCount(count($expected_backups_deleted), $deleted_backups, $assert_message);

    foreach ($expected_backups_deleted as $key => $value) {
      $this->assertArrayHasKey($key, $deleted_backups, 'Deleted Backups contains key');
      $this->assertEquals($deleted_backups[$key]['date'], $value['date'], 'Deleted Backups contains Value');
    }

  }

  /**
   * Dataprovider for testDeleteBackups.
   */
  public function getBackupsScenarios() {
    $date_format = 'Y-m-d\T00:00:00P';
    return [
      [
        1,
        'number_to_keep',
        'Deletes 3 backups',
        [
          12345 => [
            'date' => date($date_format, strtotime('-20 days')),
          ],
          22345 => [
            'date' => date($date_format, strtotime('-15 days')),
          ],
          32345 => [
            'date' => date($date_format, strtotime('-10 days')),
          ],
        ],
      ],
      [
        3,
        'number_to_keep',
        'Deletes only one backup',
        [
          12345 => [
            'date' => date($date_format, strtotime('-20 days')),
          ],
        ],
      ],
      [
        5,
        'number_to_keep',
        'Should keep all backups',
        [],
      ],
      [
        0,
        'number_to_keep',
        'The limit always should be upper 0.',
        [],
      ],
      [
        1,
        'time_to_keep',
        'Keep only one day, but never allows to delete all backups.',
        [
          12345 => [
            'date' => date($date_format, strtotime('-20 days')),
          ],
          22345 => [
            'date' => date($date_format, strtotime('-15 days')),
          ],
          32345 => [
            'date' => date($date_format, strtotime('-10 days')),
          ],
        ],
      ],
      [
        20,
        'time_to_keep',
        'Deletes backup -20 days.',
        [
          12345 => [
            'date' => date($date_format, strtotime('-20 days')),
          ],
        ],
      ],
      [
        30,
        'time_to_keep',
        'Do not delete any backup.',
        [],
      ],
      [
        0,
        'time_to_keep',
        'The limit always should be upper 0.',
        [],
      ],
    ];
  }

}
